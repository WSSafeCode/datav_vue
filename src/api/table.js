import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/vue-admin-template/table/list',
    method: 'get',
    params
  })
}

export function getInfo(params){
  return request({
    url:'http://localhost:8081/getInfo',
    method:'post',
    params
  })
}

export function deleteInfo(params){
  return request({
    url:'http://localhost:8081/deleteInfo',
    method:'post',
    params
  })
}

export function getUser(params){
  return request({
    url:'http://localhost:8081/getUser',
    method:'post',
    params
  })
}
